# caesarean_section_study
Statistical comparison of 2 Caesarian section techniques: STARK vs FAUCS.

**Run the project**
To run the project, download the repository and open *main.Rmd* in the R Studio. *"faucs_prospective study.csv"* source file should be in the same directory. Otherwise, the path to this file should be specified.

**See the results** 
Results of modeling are in the *results.html* file

**Variables list**

**|№|type|Name|**

|3 |category|Caesarean_section_technique_0_FAUCS_1_GOLD_STANDARD|
|4 |num/int|Patient's_age|
|5 |num/int|Patient's_height|
|6 |num/int|Patient_weight|
|7 |num/int|Patient's_body_mass_index|
|8 |num/int|Gestity|
|9 |num/int|Parity|
|10|num/int|Number_of_previous_cesarean_scars|
|11|category|Timing_of_the_previous_Caesarean_section_in_relation_to_labour_0_scheduled_1_emergency|
|12|category|Scarred_uterus_?_0_No_1_yes|
|13|num/int||Term_of_current_cesarean_section_in_weeks_of_amenorrhea|
|14|num/int||Number_of_fetuses_in_the_current_pregnancy|
|15|category|Sex_of_the_child_0_Male_1_Female|
|16|num/int|Child's_weight|
|17|num/int|APGAR_score_at_5_min|
|18|category|Indication_of_Caesarean_section|
|19|category|Timing_of_Caesarean_section_0_Scheduled_1_emergency|
|20|category|Name_of_the_surgeon|
|21|category|Anesthesia_0_spinal_anesthesia_1_general_anesthesia|
|22|num/int|Doses_of_Bupivacaine|
|23|num/int|Doses_of_morphine|
|25|category|Excision_of_the_old_scar_0_no_1_yes|
|28|category|Uterovesical_peritoneum_dissection_0_no_1_yes|
|29|category|Fetal_position_during_the_actual_cesarean_0_head_down_1_breech|
|31|category|Use_of_instruments_for_extraction_0_no_1_forceps_2_spatulas|
|33|category|Use_of_the_Winnerflow_to_push_the_child_outside_0_no_1_yes|
|41|category|Mode_of_delivery_of_the_placenta_0_directed_1_artificial|
|42|category|Appearance_of_the_placenta_0_normally_inserted_1_placenta_praevia|
|43|category|Hysterorraphy_0_surjet_1_purse_string|
|44|category|Performing_abdominal_cleansing_0_no_1_yes|
|45|category|Closure_of_the_subcutaneous_layer_0_no_1_yes|
|46|category|Skin_closure_0_surjet_1_separated_stitches_2_glue_skin|
|24|category|Bladder_catheterization_0_no_1_clamped_2_yes_(non_ordered_variables)|
|26|category|Dissection_to_the_uterus_0_easy_1_difficult|
|27|category|Extra-peritoneal_approach_0_yes_1_breach_2_intraperitoneal_3_intraperitoneal_by_accident|
|30|category|Difficulty_felt_by_the_operator_to_extract_the_baby_0_easy_1_difficult|
|32|category|Maneuvers_used_to_extract_baby_0_no_1_Mauriceau_2_forceps_for_head_retention_3_forceps_and_mauriceau|
|34|category|Manual_expression_on_the_uterine_fundus_to_get_the_baby_out_0_no_1_yes|
|35|num/int|Delay_between_skin_incision_and_hysterotomy|
|36|num/int|Delay_between_hysterotomy_and_fetal_extraction_(minutes)|
|37|num/int|Duration_of_skin_to_skin_in_the_operating_room_(minutes)|
|38|num/int|Total_duration_of_the_operation_from_opening_to_closing_the_skin_(minutes)|
|39|num/int|Delay_between_the_end_of_the_operation_and_the_first_spontaneous_urination|
|40|num/int|Time_of_first_stand_up_(in_hours)|
|47|num/int|Blood_loss_(mL)|
|48|category|Complications_observed_0_none_1_bladder_injury_2_maternal_Bleeding_3_latero_pre_vesical_4_Other|
|50|category|Side_effects_of_rachial_anesthesia_0_none_1_symp_bloc_2_nauseas_3_nauseas_vomit_4_general_anesthesia|
|51|category|Pain_felt_during_the_operation_0_no_1_yes_2_general_anesthesia|
|52|category|Impression_that_the_mother_was_actively_involved_in_the_delivery|
|53|category|Help_required_by_patient_on_first_stand_up_0_no_1_yes|
|54|category|Dizziness_felt_on_the_first_stand_up_0_no_1_weak_2_intenses|
|55|num/int|Pain_felt_on_the_first_stand_up|
|56|num/int|Transit_recovery_time_(days)|
|57|num/int|Time_between_the_end_of_the_operation_and_the_first_full_meal_(hours)|
|58|num/int|Post-operative_pain_out_of_ten_points|
|59|category|Post-operative_analgesics_0_none_1_rectal_2_per_os_3_per_os_and_rectal_4_iv_and_rectal_(ordered)|
|60|category|Anticoagulants_by_low_molecular_weight_heparin_0_no_1_yes|
|61|category|Need_help_carrying_the_baby_0_no_1_yes_hospitalisation_in_pediatric_department|
|62|category|Delay_between_intervention_and_first_breastfeeding_0_first_day_1_second_day_2_no_breastfeed_(2)|
|63|category|Difficulty_to_breastfeed_0_easy_1_difficult_2_no_breastfeed_(pediatric_department)|
|64|category|Effective_date_of_discharge_from_the_hospital|

|1 |Anonymous_number|
|49|Comments_on_complications|
|2 |Date_of_Caesarean_section|

